-module(create_tables).

-include_lib("stdlib/include/qlc.hrl").

-export([
  init_tables/0,
  insert_user/3,
  insert_project/2,
  select_users/0
]).
-record(user, {
  id,
  name
}).

-record(project, {
  title,
  description
}).

-record(contributor, {
  user_id, project_title
}).

init_tables() ->
  mnesia:create_table(user,
    [{attributes, record_info(fields, user)}]),
  mnesia:create_table(project,
    [{attributes, record_info(fields, project)}]),
  mnesia:create_table(contributor,
    [{type, bag}, {attributes, record_info(fields, contributor)}]).

insert_user(Id, Name, ProjectTitles) when ProjectTitles =/= [] ->
  User = #user{id = Id, name = Name},
  Fun = fun() ->
    mnesia:write(User),
    lists:foreach(fun(Title) ->
      [#project{title = Title}] = mnesia:read(project, Title),
      mnesia:write(#contributor{user_id = Id, project_title = Title})
    end, ProjectTitles)
  end,
  mnesia:transaction(Fun).

insert_project(Title, Description) ->
  mnesia:dirty_write(#project{title = Title, description = Description}).

select_users() ->
  mnesia:transaction(fun() ->
%    mnesia:select(user, [{#user{id = '$1', name = martin}, [], ['$1']}])
    Table = mnesia:table(user),
    QueryHandle = qlc:q([{U#user.id, U#user.name} || U <- Table, U#user.name =:= martin]),
    qlc:eval(QueryHandle)
  end).
