-module(jp_app).
-behavior(application).

%%% API
-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
  case jp_sup:start_link() of
    {ok, Pid} ->
      {ok, Pid};
    Other ->
      {error, Other}
  end.

stop(_State) ->
  ok.
