-module(jp_server).
-behavior(gen_server).

%% API
-export([ parse_document/1,
  start_link/0
]).

%% gen_server
-export([
  init/1, handle_call/3, handle_cast/2, handle_info/2,
  terminate/2, code_change/3
]).

-define(APPNAME, "json_parser").
-define(SERVER, ?MODULE).

-record(state, {port}).

%%% API
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

parse_document(Data) when is_binary(Data) ->
  gen_server:call(?SERVER, {parse_document, Data}).

%%% gen_server
init([]) ->
  {ok, #state{port=create_port()}}.

handle_call({parse_document, Msg}, _From, #state{port=Port}=State) ->
  io:format("Handle call entered with Msg ~p~n", [term_to_binary(Msg)]),
  port_command(Port, term_to_binary(Msg)),
  io:format("Msg sent~n"),
  receive
    {Port, {data, Data}} ->
      {reply, binary_to_term(Data), State};
    Other ->
      {error, Other}
  end.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info({Port, {exit_status, Status}}, #state{port=Port}=State) ->
  error_logger:format("port exited with status ~p; restarting", [Status]),
  {stop, port_shutdown, State};
handle_info(_Msg, _State) ->
  ok.

code_change(_Msg, _Vsn, _State) ->
  ok.

terminate(_Reason, _State) ->
  ok.

%%% priv methods
create_port() ->
  case code:priv_dir(?APPNAME) of
    {error, _} ->
      error_logger:format("~w priv dir not found~n", [?APPNAME]),
      exit(error);
    PrivDir ->
      case erl_ddll:load(PrivDir, "jp_driver") of
        ok -> ok;
        Other -> exit(Other)
      end,
      open_port({spawn, "jp_driver"}, [binary])
  end.
