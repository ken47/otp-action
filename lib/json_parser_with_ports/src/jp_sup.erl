-module(jp_sup).
-behavior(supervisor).

%%% API
-export([start_link/0]).

%%% Supervisor CB
-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->
  JpServer = {jp_server, {jp_server, start_link, []},
    transient, 2000, worker, [jp_server]},
  Children = [JpServer],
  RestartStrategy = {one_for_one, 4, 3600},
  {ok, {RestartStrategy, Children}}.
