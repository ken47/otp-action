-module(rd_event_mgr).

-export([
  start_link/0,
  add_handler/2,
  delete_handler/2,
  lookup/1,
  found_node/1
]).

-define(SERVER, ?MODULE).

start_link() ->
  gen_event:start_link({local, ?SERVER}).

add_handler(Handler, Args) ->
  gen_event:add_handler(?SERVER, Handler, Args).

delete_handler(Handler, Args) ->
  gen_event:delete_handler(?SERVER, Handler, Args).

lookup(Key) ->
  gen_event:notify(?SERVER, {lookup, Key}).

found_node(Node) ->
  gen_event:notify(?SERVER, {found, Node}).
