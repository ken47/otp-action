-module(rd_event_logger).
-behavior(gen_event).
-export([add_handler/0, delete_handler/0]).

-export([
  init/1, handle_event/2, handle_call/2,
  handle_info/2, terminate/2, code_change/3
]).

-record(state, {}).

add_handler() ->
  rd_event_mgr:add_handler(?MODULE, []).

delete_handler() ->
  rd_event_mgr:delete_handler(?MODULE, []).

init([]) ->
  {ok, #state{}}.

handle_event({found, Type}, State) ->
  error_logger:info_msg("found resource(~w)~n", [Type]),
  {ok, State}.

handle_call(_Request, State) ->
  {ok, ok, State}.

handle_info(_Info, State) ->
  {ok, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

