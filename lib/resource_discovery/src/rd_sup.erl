-module(rd_sup).
-behavior(supervisor).

%%% API
-export([start_link/0]).

%%% Supervisor CB
-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->
  RdServer = {rd_server, {rd_server, start_link, []},
    permanent, 2000, worker, [rd_server]},
  RdEventMgr = {rd_event_mgr, {rd_event_mgr, start_link, []},
    permanent, 2000, worker, [rd_event_mgr]},
  Children = [RdServer, RdEventMgr],
  RestartStrategy = {one_for_one, 4, 3600},
  {ok, {RestartStrategy, Children}}.
