-module(json_parser).
-on_load(init/0).
-export([parse_document/1]).
-define(APPNAME, "json_parser").

init() ->
  case code:priv_dir(?APPNAME) of
    {error, _} ->
      error_logger:format("~w priv dir not found~n", [?APPNAME]),
      exit(error);
    PrivDir ->
      erlang:load_nif(filename:join([PrivDir, "jp_nifs"]), 0)
  end.

parse_document(_Data) ->
  erlang:nif_error(nif_not_loaded).
