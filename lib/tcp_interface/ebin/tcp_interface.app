{application, tcp_interface,
[
  {description, "tcp interface for simple_cache"},
  {vsn, "0.1.0"},
  {modules, [
    ti_app,
    ti_sup,
    ti_server,
    tcp_interface
  ]},
  {registered, [ti_sup]},
  {mod, {ti_app, []}},
  {applications, [
    kernel,
    stdlib,
    sasl
  ]}
]
}.
