-module(hi_sup).
-behavior(supervisor).

%% API
-export([start_link/1]).

%% Supervisor CB
-export([init/1]).

-define(SERVER, ?MODULE).

start_link(Port) ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, [Port]).

init([Port]) ->
  HiServer = {hi_server, {hi_server, start_link, [Port]},
    permanent, brutal_kill, worker, [hi_server]},
  Children = [HiServer],
  RestartStrategy = {one_for_one, 4, 60 * 60},
  {ok, {RestartStrategy, Children}}.
